import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';

@Component({
    selector: 'employee-list',
    templateUrl: 'employee.component.html',
    styleUrls:['./employee.component.scss']
})

export class EmployeeListComponent implements OnInit {
    
    employeeSummaryList: any;

    constructor(private employeeService: EmployeeService, private router: Router, private spinner: NgxSpinnerService,private notifierService: NotifierService) {  }

    ngOnInit() {
        //Show loader
        this.spinner.show();
        this.employeeService.getAllEmployeesSummary().subscribe(data => {
            this.employeeSummaryList = data;
        }, error => {
            var e:any = error;
            this.notifierService.notify( 'error', e.message );
            this.spinner.hide(); 
        }, () => {
            //Hide loader
            this.spinner.hide(); 
        });
    }

    public navigateTimesheetView(employeeId:number)
    {
        this.router.navigate(['/Timesheet'], { queryParams: { empid: employeeId } });
    }
}