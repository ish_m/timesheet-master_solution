import { Component, OnInit, Directive, ElementRef, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { forkJoin } from 'rxjs';  // RxJS 6 syntax
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbActiveModal, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { List } from 'linqts';
import { NotifierService } from 'angular-notifier';

import { EmployeeService } from '../services/employee.service';
import { TaskService } from '../services/task.service';
import { TimesheetService } from '../services/timesheet.service';

import * as moment from 'moment';
declare var jQuery: any;

@Component({
  selector: 'app-timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: ['./timesheet.component.scss']
})
export class TimesheetComponent implements OnInit {

  //Variables
  selectedEmployeeId: number;
  allEmployeeList: any;
  allTaskList: any;
  timesheetItemList: TimesheetItem[] = [];

  constructor(private activatedRoute: ActivatedRoute, private employeeService: EmployeeService, private taskService: TaskService, private timesheetService: TimesheetService, private spinner: NgxSpinnerService, private modalService: NgbModal, private router: Router, private notifierService: NotifierService) { }

  ngOnInit() {
    //Getting selected employee id from query string
    this.selectedEmployeeId = parseInt(this.activatedRoute.snapshot.queryParams["empid"]);

    //Show loader
    this.spinner.show();
    forkJoin(this.employeeService.getAllEmployees(), this.taskService.getAllTasks()).subscribe(responseList => {
      this.allEmployeeList = responseList[0];
      this.allTaskList = responseList[1];
    }, error => {
      var e: any = error;
      this.notifierService.notify('error', e.message);
      this.spinner.hide();
    }, () => {
      //Hide loader
      this.spinner.hide();
    });

    //Initializing Bootstrap Datepicker
    jQuery("#weekpicker input").datetimepicker({
      format: 'YYYY-MM-DD',
      useCurrent:true
    });

    var self = this;
    //Get the value of Start and End of Week
    jQuery('#weekpicker input').on('dp.change', function (e) {
      var date = jQuery("#weekpicker input").val();

      var week_from = moment(date).startOf('week');
      var week_til = moment(date).startOf('week').add(6, 'd');

       //Appending the Date & Month for the timesheet day headers
       jQuery('#sun_date_ph').text(" | " + moment(date).startOf('week').format("D/M"));
       jQuery('#mon_date_ph').text(" | " + moment(date).startOf('week').add(1, 'd').format("D/M"));
       jQuery('#tue_date_ph').text(" | " + moment(date).startOf('week').add(2, 'd').format("D/M"));
       jQuery('#wed_date_ph').text(" | " + moment(date).startOf('week').add(3, 'd').format("D/M"));
       jQuery('#thu_date_ph').text(" | " + moment(date).startOf('week').add(4, 'd').format("D/M"));
       jQuery('#fri_date_ph').text(" | " + moment(date).startOf('week').add(5, 'd').format("D/M"));
       jQuery('#sat_date_ph').text(" | " + moment(date).startOf('week').add(6, 'd').format("D/M"));

      jQuery("#weekpicker input").val(week_from.format("MMMM Do YYYY") + " - " + week_til.format("MMMM Do YYYY"));

      self.loadEmployeeTimesheet();
    });

    //Preventing user to enter values in datepicker
    jQuery('#weekpicker input').keypress(function (e) {
      e.preventDefault();
    });

    //Setting current date for week picker
    jQuery("#weekpicker input").datetimepicker("defaultDate", new Date());

    this.loadEmployeeTimesheet();
  }

  //Load timesheet on employee change
  onEmployeeDropdownChange() {
    this.loadEmployeeTimesheet();
  }

  //Load timesheet on week change
  onWeekChange() {
    this.loadEmployeeTimesheet();
  }

  //Opening the add task modal popup
  open(content) {
    const modalRef = this.modalService.open(NgbdModalContent, {}).result.then((result) => {
      let taskItem = new List<TimesheetItem>(this.timesheetItemList).Where(x => x.TaskId == result).FirstOrDefault();
      if (taskItem == null) {
        debugger;
        let task = new List<any>(this.allTaskList).Where(x => x.id == result).FirstOrDefault();

        var timesheetItem = new TimesheetItem();
        timesheetItem.EmployeeId = this.selectedEmployeeId;
        timesheetItem.WeekStartDate = moment(jQuery("#weekpicker input").datetimepicker("date")).startOf('week').format("YYYY-MM-DD");
        timesheetItem.TaskId = task != null ? task.id : 0;
        timesheetItem.TaskName = task != null ? task.name : 0;
        this.timesheetItemList.push(timesheetItem);
      }
      else {
        this.notifierService.notify('warning', "Selected task already added, please select a different task.");
      }
    });
  }

  loadEmployeeTimesheet() {

    //Calculatin 'Week Start' date & 'Week End' date
    var selectedDate = jQuery("#weekpicker input").datetimepicker("date");
    var weekStartDate = moment(selectedDate).startOf('week').format("YYYY-MM-DD");
    var weekEndDate = moment(selectedDate).startOf('week').add(6, 'd').format("YYYY-MM-DD");

    //Show loader
    this.spinner.show();

    //Retrieving employee timesheet data & creating the the VM
    this.timesheetService.getEmployeeTimesheets(this.selectedEmployeeId, weekStartDate, weekEndDate).subscribe(resultList => {
      var result: any = [];
      result = resultList;

      this.timesheetItemList = [];
      result.forEach(item => {
        var timesheetItem: TimesheetItem = new TimesheetItem();
        timesheetItem.EmployeeId = item.employeeId;
        timesheetItem.TaskId = item.taskId;
        timesheetItem.TaskName = item.taskName;
        timesheetItem.WeekStartDate = item.weekStartDate;
        timesheetItem.SundayEffort = item.sundayEffort;
        timesheetItem.MondayEffort = item.mondayEffort;
        timesheetItem.TuesdayEffort = item.tuesdayEffort;
        timesheetItem.WednesdayEffort = item.wednesdayEffort;
        timesheetItem.ThursdayEffort = item.thursdayEffort;
        timesheetItem.FridayEffort = item.fridayEffort;
        timesheetItem.SaturdayEffort = item.saturdayEffort;

        this.timesheetItemList.push(timesheetItem);
      });
    }, error => {
      var e: any = error;
      this.notifierService.notify('error', e.message);
      this.spinner.hide();
    }, () => {
      //Hide loader
      this.spinner.hide();
    });
  }

  saveTimesheet() {

    //Validating whether effort is exceeding 24H for any day
    var sundayTotalEffort = new List<TimesheetItem>(this.timesheetItemList).Sum(t => t.SundayEffort);
    if (sundayTotalEffort >= 24) {
      this.notifierService.notify('warning', "Cannot proceed, total effort for Sunday exceeding 24 hours");
      return;
    }

    var mondayTotalEffort = new List<TimesheetItem>(this.timesheetItemList).Sum(t => t.MondayEffort);
    if (mondayTotalEffort >= 24) {
      this.notifierService.notify('warning', "Cannot proceed, total effort for Tuesday exceeding 24 hours");
      return;
    }

    var tuesdayTotalEffort = new List<TimesheetItem>(this.timesheetItemList).Sum(t => t.TuesdayEffort);
    if (sundayTotalEffort >= 24) {
      this.notifierService.notify('warning', "Cannot proceed, total effort for Tuesday exceeding 24 hours");
      return;
    }

    var wednesdayTotalEffort = new List<TimesheetItem>(this.timesheetItemList).Sum(t => t.WednesdayEffort);
    if (wednesdayTotalEffort >= 24) {
      this.notifierService.notify('warning', "Cannot proceed, total effort for Wednesday exceeding 24 hours");
      return;
    }

    var thursdayTotalEffort = new List<TimesheetItem>(this.timesheetItemList).Sum(t => t.ThursdayEffort);
    if (thursdayTotalEffort >= 24) {
      this.notifierService.notify('warning', "Cannot proceed, total effort for Thursday exceeding 24 hours");
      return;
    }

    var fridayTotalEffort = new List<TimesheetItem>(this.timesheetItemList).Sum(t => t.FridayEffort);
    if (fridayTotalEffort >= 24) {
      this.notifierService.notify('warning', "Cannot proceed, total effort for Friday exceeding 24 hours");
      return;
    }

    var saturdayTotalEffort = new List<TimesheetItem>(this.timesheetItemList).Sum(t => t.SaturdayEffort);
    if (saturdayTotalEffort >= 24) {
      this.notifierService.notify('warning', "Cannot proceed, total effort for Saturday exceeding 24 hours");
      return;
    }

    if(this.timesheetItemList.length==0){
      this.notifierService.notify('warning', "Cannot proceed, Please add task and continue.");
      return;
    }

    this.spinner.show();
    this.timesheetService.saveTimesheet(this.timesheetItemList).subscribe(response => {
      var res: any = response;
      if (res.isSuccess) {
        this.notifierService.notify('success', "Timesheet saved successfully.");
      }
      else {
        this.notifierService.notify('error', res.validationMessage);
      }
    }, error => {
      var e: any = error;
      this.notifierService.notify('error', e.message);
      this.spinner.hide();
    }, () => {
      this.spinner.hide();
    });
  }

  //Navigate back to employee list page
  goBackToList() {
    this.router.navigate(['/EmployeeList'], { queryParams: {} });
  }
}

//Add task modal popup
@Component({
  selector: 'ngbd-modal-content',
  styleUrls: ['./timesheet.component.scss'],
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Add Task</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
    <div class="col-md-6">
    <form class="form-inline">
      <div class="form-group">
        <label for="ddlAllTask">Select Task : </label>
        <select id="ddlAllTask" name="ddlAllTask" [(ngModel)]="selectedTaskId">
          <option *ngFor="let t of allTaskList" [value]="t.id">{{t.name}}</option>
        </select>
      </div>
    </form>
  </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-success btn-sm" (click)="activeModal.close(selectedTaskId)">Add</button>
    </div>
  `
})
export class NgbdModalContent implements OnInit {

  allTaskList: any;
  selectedTaskId: number;

  constructor(public activeModal: NgbActiveModal, private taskService: TaskService, private spinner: NgxSpinnerService, private notifierService: NotifierService) { }

  ngOnInit() {
    this.taskService.getAllTasks().subscribe(responseList => {
    this.allTaskList = responseList; this.selectedTaskId = responseList[0].id
    }, error => {
      var e: any = error;
      this.notifierService.notify('error', e.message);
    }, () => {
    })
  }

}

//Directive for textbox to only accept numbers
@Directive({
  selector: '[NumberOnly]'
})
export class NumberOnlyDirective {

  private regex: RegExp = new RegExp(/^-?[0-9]+(\.[0-9]*){0,1}$/g);

  private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home','Left','Right'];

  constructor(private el: ElementRef) {
  }
  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }
    let current: string = this.el.nativeElement.value;
    let next: string = current.concat(event.key);
    if (next && !String(next).match(this.regex)) {
      event.preventDefault();
    }
  }
}

//Timesheet modal
export class TimesheetItem {
  TaskId: number;
  TaskName: string;
  EmployeeId: number;
  WeekStartDate: any;
  SundayEffort: number = 0;
  MondayEffort: number = 0;
  TuesdayEffort: number = 0;
  WednesdayEffort: number = 0;
  ThursdayEffort: number = 0;
  FridayEffort: number = 0;
  SaturdayEffort: number = 0;
}