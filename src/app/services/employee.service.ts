import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EmployeeService {
    
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    //Returns employee effort summary data for employee list grid
    getAllEmployeesSummary() {
        return this.http.get(this.baseapi + "/employee/getallemployeessummary");
    }

    //Returns all employees in the system
    getAllEmployees() {
        return this.http.get(this.baseapi + "/employee/getallemployees");
    }
}