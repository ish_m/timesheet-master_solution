import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TimesheetService {
  private baseapi = environment.apiUrl;
  constructor(private http: HttpClient) { }

  //Insert or update timesheet 
  saveTimesheet(timesheetList: any) {
    return this.http.post(this.baseapi + "/timesheet/InsertUpdateTimesheets", timesheetList);
  }

  //Returns filtered timesheet by employee and week
  getEmployeeTimesheets(employeeId: number, weekStartDate: string, weekEndDate: string) {
    return this.http.get(this.baseapi + "/timesheet/getemployeetimesheets/" + employeeId + "/" + weekStartDate + "/" + weekEndDate);
  }
}
