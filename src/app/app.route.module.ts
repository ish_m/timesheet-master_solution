import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';

import { EmployeeListComponent } from './employee/employee.component';
import { TimesheetComponent } from './timesheet/timesheet.component';

const routes: Route[] = [
    { path: '', redirectTo: 'EmployeeList', pathMatch: 'full' },
    { path: 'EmployeeList', component: EmployeeListComponent },
    { path: 'Timesheet', component: TimesheetComponent }
];

@NgModule({
    imports: [
      RouterModule.forRoot(routes)
    ],
    exports: [RouterModule],
    declarations: []
  })
  export class AppRoutingModule { }