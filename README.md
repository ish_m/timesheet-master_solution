# timesheet-master_solution

Angular App of GAC Timesheet assessment

Download the repo and perform a **npm i** to install the packages and then **ng serve** to run the App

Demo video of the application also included in the repo **(timesheet_demo.mp4)**

